﻿using OnlineCV.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineCV.Tests
{
    class StoppedClock : IClock
    {
        public DateTime Now => new DateTime(2019, 1, 1, 12, 0, 0);
    }
}
