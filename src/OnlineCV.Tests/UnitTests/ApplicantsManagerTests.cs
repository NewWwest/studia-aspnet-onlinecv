﻿using Microsoft.EntityFrameworkCore;
using Moq;
using OnlineCV.Data;
using OnlineCV.Data.Repositories;
using OnlineCV.Models.JobContenders;
using OnlineCV.Models.JobOffer;
using OnlineCV.Models.Profile;
using OnlineCV.Services;
using OnlineCV.Tests.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace OnlineCV.Tests.UnitTests
{
    public class ApplicantsManagerTests
    {
        [Fact]
        public  void ManagerWillThrowIfNoProfileWasProvided()
        {
            var dataSource = new Mock<IDatabaseContext>();
            var jobContendersMock = new Mock<DbSet<JobContender>>();
            var jobContenders = JobContendersTestData.TestJobContenders.AsQueryable();
            jobContendersMock.As<IAsyncEnumerable<JobContender>>()
                .Setup(m => m.GetEnumerator())
                .Returns(new TestAsyncEnumerator<JobContender>(jobContenders.GetEnumerator()));
            jobContendersMock.As<IQueryable<JobContender>>()
                .Setup(m => m.Provider)
                .Returns(new TestAsyncQueryProvider<JobContender>(jobContenders.Provider));
            jobContendersMock.As<IQueryable<JobContender>>().Setup(m => m.Expression).Returns(jobContenders.Expression);
            jobContendersMock.As<IQueryable<JobContender>>().Setup(m => m.ElementType).Returns(jobContenders.ElementType);
            jobContendersMock.As<IQueryable<JobContender>>().Setup(m => m.GetEnumerator()).Returns(jobContenders.GetEnumerator());
            dataSource.Setup(m => m.JobContenders).Returns(jobContendersMock.Object);

            var profileRepoMock = new Mock<IRepository<string, ProfileEntity>>();
            profileRepoMock.Setup(a => a.GetByPK(It.IsAny<string>()))
                .Returns<string>(r => ProfileTestData.TestProfiles.FirstOrDefault(p=>p.UserID==r));


            var jobOfferRepoMock = new Mock<IRepository<int, JobOfferEntity>>();
            jobOfferRepoMock.Setup(a => a.GetByPK(It.IsAny<int>()))
                .Returns<int>(r => JobOfferTestData.TestJobs.FirstOrDefault(j => j.Id == r));

            //Act
            var repo = new ApplicantsManager(jobOfferRepoMock.Object, profileRepoMock.Object, dataSource.Object);
            //assert
            Assert.Throws<Exception>(() =>repo.Apply("invalidUserID", 1));
        }

        [Fact]
        public void ManagerWillThrowIfJobDoesntExist()
        {
            var dataSource = new Mock<IDatabaseContext>();
            var jobContendersMock = new Mock<DbSet<JobContender>>();
            var jobContenders = JobContendersTestData.TestJobContenders.AsQueryable();
            jobContendersMock.As<IAsyncEnumerable<JobContender>>()
                .Setup(m => m.GetEnumerator())
                .Returns(new TestAsyncEnumerator<JobContender>(jobContenders.GetEnumerator()));
            jobContendersMock.As<IQueryable<JobContender>>()
                .Setup(m => m.Provider)
                .Returns(new TestAsyncQueryProvider<JobContender>(jobContenders.Provider));
            jobContendersMock.As<IQueryable<JobContender>>().Setup(m => m.Expression).Returns(jobContenders.Expression);
            jobContendersMock.As<IQueryable<JobContender>>().Setup(m => m.ElementType).Returns(jobContenders.ElementType);
            jobContendersMock.As<IQueryable<JobContender>>().Setup(m => m.GetEnumerator()).Returns(jobContenders.GetEnumerator());
            dataSource.Setup(m => m.JobContenders).Returns(jobContendersMock.Object);

            var profileRepoMock = new Mock<IRepository<string, ProfileEntity>>();
            profileRepoMock.Setup(a => a.GetByPK(It.IsAny<string>()))
                .Returns<string>(r => ProfileTestData.TestProfiles.FirstOrDefault(p => p.UserID == r));


            var jobOfferRepoMock = new Mock<IRepository<int, JobOfferEntity>>();
            jobOfferRepoMock.Setup(a => a.GetByPK(It.IsAny<int>()))
                .Returns<int>(r => JobOfferTestData.TestJobs.FirstOrDefault(j => j.Id == r));

            //Act
            var repo = new ApplicantsManager(jobOfferRepoMock.Object, profileRepoMock.Object, dataSource.Object);
            //assert
            Assert.Throws<Exception>(() => repo.Apply("1", 420420));
        }

        [Fact]
        public void ManagerWillAttempToAddNewAplicationWhenOneIsPresent()
        {
            var dataSource = new Mock<IDatabaseContext>();
            var jobContendersMock = new Mock<DbSet<JobContender>>();
            var jobContenders = JobContendersTestData.TestJobContenders.AsQueryable();
            jobContendersMock.As<IAsyncEnumerable<JobContender>>()
                .Setup(m => m.GetEnumerator())
                .Returns(new TestAsyncEnumerator<JobContender>(jobContenders.GetEnumerator()));
            jobContendersMock.As<IQueryable<JobContender>>()
                .Setup(m => m.Provider)
                .Returns(new TestAsyncQueryProvider<JobContender>(jobContenders.Provider));
            jobContendersMock.As<IQueryable<JobContender>>().Setup(m => m.Expression).Returns(jobContenders.Expression);
            jobContendersMock.As<IQueryable<JobContender>>().Setup(m => m.ElementType).Returns(jobContenders.ElementType);
            jobContendersMock.As<IQueryable<JobContender>>().Setup(m => m.GetEnumerator()).Returns(jobContenders.GetEnumerator());
            dataSource.Setup(m => m.JobContenders).Returns(jobContendersMock.Object);

            var profileRepoMock = new Mock<IRepository<string, ProfileEntity>>();
            profileRepoMock.Setup(a => a.GetByPK(It.IsAny<string>()))
                .Returns<string>(r => ProfileTestData.TestProfiles.FirstOrDefault(p => p.UserID == r));


            var jobOfferRepoMock = new Mock<IRepository<int, JobOfferEntity>>();
            jobOfferRepoMock.Setup(a => a.GetByPK(It.IsAny<int>()))
                .Returns<int>(r => JobOfferTestData.TestJobs.FirstOrDefault(j => j.Id == r));

            //Act
            var repo = new ApplicantsManager(jobOfferRepoMock.Object, profileRepoMock.Object, dataSource.Object);
            //assert
            repo.Apply("1", 1);//Adding jobContenders was not mocked, thus this would fail if manager tried to add another aplication
        }
    }
}
