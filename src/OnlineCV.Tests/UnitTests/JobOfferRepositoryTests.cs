﻿using Microsoft.EntityFrameworkCore;
using Moq;
using OnlineCV.Data;
using OnlineCV.Data.Repositories;
using OnlineCV.Models.JobOffer;
using OnlineCV.Services;
using OnlineCV.Tests.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace OnlineCV.Tests.UnitTests
{
    public class JobOfferRepositoryTests
    {
        
        
        [Fact]
        public async Task RepositoryDoesntShowHiddenJobs()
        {
            //arrange
            var dataSource = new Mock<IDatabaseContext>();
            var jobOffersMock = new Mock<DbSet<JobOfferEntity>>();
            var jobs = JobOfferTestData.TestJobs.AsQueryable();

            jobOffersMock.As<IAsyncEnumerable<JobOfferEntity>>()
                .Setup(m => m.GetEnumerator())
                .Returns(new TestAsyncEnumerator<JobOfferEntity>(jobs.GetEnumerator()));

            jobOffersMock.As<IQueryable<JobOfferEntity>>()
                .Setup(m => m.Provider)
                .Returns(new TestAsyncQueryProvider<JobOfferEntity>(jobs.Provider));

            jobOffersMock.As<IQueryable<JobOfferEntity>>().Setup(m => m.Expression).Returns(jobs.Expression);
            jobOffersMock.As<IQueryable<JobOfferEntity>>().Setup(m => m.ElementType).Returns(jobs.ElementType);
            jobOffersMock.As<IQueryable<JobOfferEntity>>().Setup(m => m.GetEnumerator()).Returns(jobs.GetEnumerator());

            dataSource.Setup(m => m.JobOffers).Returns(jobOffersMock.Object);

            //Act
            var repo = new JobOfferRepository(dataSource.Object, new DateTimeClock());
            var returnedJobs = await repo.GetRangeAsync(10, 10, "", "");

            //assert
            Assert.Null(returnedJobs.FirstOrDefault(j => j.Id == 4));
        }

        [Fact]
        public async Task RepositoryDoesntShowExpiredJobs()
        {
            //arrange
            var dataSource = new Mock<IDatabaseContext>();
            var jobOffersMock = new Mock<DbSet<JobOfferEntity>>();
            var jobs = JobOfferTestData.TestJobs.AsQueryable();

            jobOffersMock.As<IAsyncEnumerable<JobOfferEntity>>()
                .Setup(m => m.GetEnumerator())
                .Returns(new TestAsyncEnumerator<JobOfferEntity>(jobs.GetEnumerator()));

            jobOffersMock.As<IQueryable<JobOfferEntity>>()
                .Setup(m => m.Provider)
                .Returns(new TestAsyncQueryProvider<JobOfferEntity>(jobs.Provider));

            jobOffersMock.As<IQueryable<JobOfferEntity>>().Setup(m => m.Expression).Returns(jobs.Expression);
            jobOffersMock.As<IQueryable<JobOfferEntity>>().Setup(m => m.ElementType).Returns(jobs.ElementType);
            jobOffersMock.As<IQueryable<JobOfferEntity>>().Setup(m => m.GetEnumerator()).Returns(jobs.GetEnumerator());

            dataSource.Setup(m => m.JobOffers).Returns(jobOffersMock.Object);

            //Act
            var repo = new JobOfferRepository(dataSource.Object, new DateTimeClock());
            var returnedJobs = await repo.GetRangeAsync(10, 10, "", "");

            //assert
            Assert.Null(returnedJobs.FirstOrDefault(j => j.Id == 3));
        }
    }
}
