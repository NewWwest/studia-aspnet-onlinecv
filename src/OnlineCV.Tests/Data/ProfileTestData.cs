﻿using OnlineCV.Models.Profile;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineCV.Tests.Data
{
    public static class ProfileTestData
    {
        public static List<ProfileEntity> TestProfiles = new List<ProfileEntity>()
        {
            new ProfileEntity()
            {
                UserID="1",
                Name="name1",
                Surname="surname1",
                PathToCv="someUrl1",
                PathToProfilePicture="someURI1"
            },
            new ProfileEntity()
            {
                UserID="2",
                Name="name2",
                Surname="surname2",
                PathToCv="someUrl2",
                PathToProfilePicture="someURI2"
            },
            new ProfileEntity()
            {
                UserID="3",
                Name="name1",
                Surname="surname1",
                PathToCv="someUrl3",
                PathToProfilePicture="someURI3"
            }
        };
    }
}
