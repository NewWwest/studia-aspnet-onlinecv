﻿using OnlineCV.Models.JobOffer;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineCV.Tests.Data
{
    public static class JobOfferTestData
    {
        private static StoppedClock stoppedClock = new StoppedClock();
        public static List<JobOfferEntity> TestJobs = new List<JobOfferEntity>()
        {
            new JobOfferEntity()
            {
                Id=1,
                Title="CorrectJob1",
                Hidden=false,
                DueDate=stoppedClock.Now.AddDays(5),
                Description="1 1",
                CreateDate=stoppedClock.Now.AddDays(-5),
                CreatorId="creator1"
            },
            new JobOfferEntity()
            {
                Id=2,
                Title="CorrectJob2",
                Hidden=false,
                DueDate=stoppedClock.Now.AddDays(15),
                Description="2 2",
                CreateDate=stoppedClock.Now.AddDays(-5),
                CreatorId="creator2"
            },
            new JobOfferEntity()
            {
                Id=3,
                Title="ExpiredJob",
                Hidden=false,
                DueDate=stoppedClock.Now.AddDays(-5),
                Description="3 3",
                CreateDate=stoppedClock.Now.AddDays(-15),
                CreatorId="creator3"
            },
            new JobOfferEntity()
            {
                Id=4,
                Title="HiddenJob",
                Hidden=true,
                DueDate=stoppedClock.Now.AddDays(5),
                Description="4 4",
                CreateDate=stoppedClock.Now.AddDays(-5),
                CreatorId="creator1"
            },
        };
    }
}
