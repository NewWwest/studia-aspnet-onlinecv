﻿using OnlineCV.Models.JobContenders;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineCV.Tests.Data
{
    public static class JobContendersTestData
    {
        public static List<JobContender> TestJobContenders = new List<JobContender>()
        {
            new JobContender()
            {
                JobId=1,
                UserId="1"
            },
            new JobContender()
            {
                JobId=2,
                UserId="1"
            },
            new JobContender()
            {
                JobId=2,
                UserId="2"
            }
        };
    }
}
