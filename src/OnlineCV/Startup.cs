﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.AzureADB2C.UI;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OnlineCV.Data;
using OnlineCV.Helpers;
using OnlineCV.Data.Repositories;
using OnlineCV.Models.JobOffer;
using OnlineCV.Models.Profile;
using OnlineCV.Models.Company;
using OnlineCV.Data.Storage;
using OnlineCV.Services;
using Swashbuckle.AspNetCore.Swagger;
using System.Reflection;
using System.IO;

namespace OnlineCV
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddJsonFile("sensitive.json.secret", optional: false)
                .AddEnvironmentVariables();

            //To access value
            //Debug.WriteLine(Configuration.GetValue<string>("ParentKey:childKey"));

            Configuration = builder.Build();

        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(sharedOptions =>
            {
                sharedOptions.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                sharedOptions.DefaultChallengeScheme = OpenIdConnectDefaults.AuthenticationScheme;
            })
            .AddAzureAdB2C(options => Configuration.Bind(Extensions.JsonPath, options))
            .AddCookie();
            services.AddMemoryCache();
            services.AddDistributedMemoryCache();
            services.AddSession();


            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDbContext<DatabaseContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));

            services.AddSingleton(Configuration);
            services.Configure<AppSettings>(Configuration.GetAppSettingsSection());
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddTransient<IRepository<int, JobOfferEntity>, JobOfferRepository>();
            services.AddTransient<IRepository<string, CompanyEntity>, CompanyRepository>();
            services.AddTransient<IRepository<string, ProfileEntity>, ProfileRepository>();
            services.AddTransient<IJobRepository, JobOfferRepository>();
            services.AddTransient<IDatabaseContext, DatabaseContext>();
            services.AddTransient<IFileStorage, BlobFileStorage>();
            
            services.AddTransient<IApplicantsManager, ApplicantsManager>();

            services.AddTransient<IClock, DateTimeClock>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Title = "OnlineCv",
                    Version = "v1",
                    Description = "Api documentation for OnlineCv project",
                    Contact = new Contact
                    {
                        Name = "Andrzej Westfalewicz",
                        Email = "westfalewicza@student.mini.pw.edu.pl"
                    }
                });
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                //app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "OnlineCvs v1");
            });

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseSession();
            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                  name: "areas",
                  template: "{area:exists}/{controller=Home}/{action=Index}/{id?}");
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
