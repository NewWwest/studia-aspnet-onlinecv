﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using OnlineCV.Data;
using OnlineCV.Data.Repositories;
using OnlineCV.Data.Storage;
using OnlineCV.Helpers;
using OnlineCV.Models;
using OnlineCV.Models.AAD;
using OnlineCV.Models.Profile;
using OnlineCV.Services;
using OnlineCV.Utils;

namespace OnlineCV.Controllers
{
    [Authorize]
    public class ProfileController : Controller
    {
        private readonly IRepository<string, ProfileEntity> _profileRepository;
        private readonly IFileStorage _fileStorage;

        public ProfileController(IRepository<string, ProfileEntity> profileRepository, IFileStorage fileStorage)
        {
            _profileRepository = profileRepository;
            _fileStorage = fileStorage;
        }

        [HttpGet]
        public async Task<ActionResult> Index()
        {
            string userID = ClaimsHelper.GetUserID(User.Claims);
            ProfileEntity profile = await _profileRepository.GetByPKAsync(userID);

            if (profile != null)
                return View(profile.ToProfileView());
            else
                return View("Edit", null);
        }

        [HttpGet]
        public async Task<ActionResult> Edit()
        {
            string userID = ClaimsHelper.GetUserID(User.Claims);
            ProfileEntity profile = await _profileRepository.GetByPKAsync(userID);

            return View(profile?.ToProfileView());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(ProfileCreate model)
        {
            if (!model.IsValid())
                return await Edit();

            string userID = ClaimsHelper.GetUserID(User.Claims);
            var entity = model.ToEntity(userID);

            //Lets Capture path to old files
            ProfileEntity oldProfile = await _profileRepository.GetByPKAsync(userID);
            string oldCvPath = oldProfile?.PathToCv;
            string oldPicturePath = oldProfile?.PathToProfilePicture;

            try
            {
                using (var cvStream = model.Cv.OpenReadStream())
                {
                    await _fileStorage.Save(cvStream, entity.PathToCv);
                }
                using (var imageStream = model.ProfileImage.OpenReadStream())
                {
                    await _fileStorage.Save(imageStream, entity.PathToProfilePicture);
                }
                await _profileRepository.AddOrUpdateAsync(entity);
                if(!string.IsNullOrWhiteSpace(oldCvPath))
                    await _fileStorage.Delete(oldCvPath);
                if (!string.IsNullOrWhiteSpace(oldPicturePath))
                    await _fileStorage.Delete(oldPicturePath);
            }
            catch
            {
                //TODO insert log into a table to investigate file desynchronization
                Debug.WriteLine("Editting Of Profile Failed");
#if DEBUG
                throw;
#else
                return await Edit();
#endif
            }

            return RedirectToAction(nameof(Index));
        }
        
    }
}