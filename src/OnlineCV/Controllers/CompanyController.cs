﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OnlineCV.Data;
using OnlineCV.Data.Repositories;
using OnlineCV.Models.Company;
using OnlineCV.Utils;

namespace OnlineCV.Controllers
{
    [Authorize]
    public class CompanyController : Controller
    {
        private readonly IRepository<string, CompanyEntity> _companyRepository;

        public CompanyController(IRepository<string, CompanyEntity> companyRepository)
        {
            _companyRepository = companyRepository;
        }

        public async Task<IActionResult> Index()
        {
            var userID = ClaimsHelper.GetUserID(User.Claims);

            var companyEntity = await _companyRepository.GetByPKAsync(userID);

            if (companyEntity == null)
                return RedirectToAction(nameof(Create));

            return View(companyEntity);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("DisplayName,Description,Url")] CompanyEntity companyEntity)
        {
            companyEntity.UserID = ClaimsHelper.GetUserID(User.Claims);
            if (ModelState.IsValid)
            {
                await _companyRepository.AddOrUpdateAsync(companyEntity);
                return RedirectToAction(nameof(Index));
            }
            return View(companyEntity);
        }

        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
                return NotFound();

            var companyEntity = await _companyRepository.GetByPKAsync(id);

            if (companyEntity == null)
                return NotFound();

            return View(companyEntity);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("DisplayName,Description,Url")] CompanyEntity companyEntity)
        {
            companyEntity.UserID = ClaimsHelper.GetUserID(User.Claims);

            if (ModelState.IsValid)
            {
                try
                {
                    await _companyRepository.AddOrUpdateAsync(companyEntity);
                }
                catch (DbUpdateConcurrencyException)
                {
                    await _companyRepository.AddOrUpdateAsync(companyEntity);
                }
                return RedirectToAction(nameof(Index));
            }

            return View(companyEntity);
        }
    }
}
