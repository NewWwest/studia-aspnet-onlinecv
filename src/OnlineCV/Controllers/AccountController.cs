﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace OnlineCV.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult EditProfile()
        {
            return View();
        }

        public ActionResult SignOut()
        {
            return View();
        }

        [Authorize]
        public ActionResult SignIn()
        {
            return View();
        }
    }
}