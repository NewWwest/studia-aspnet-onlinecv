﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OnlineCV.Data;
using OnlineCV.Data.Repositories;
using OnlineCV.Models.Company;
using OnlineCV.Models.JobContenders;
using OnlineCV.Models.JobOffer;
using OnlineCV.Models.Profile;
using OnlineCV.Services;
using OnlineCV.Utils;

namespace OnlineCV.Controllers
{
    [Authorize]
    public class JobOfferController : Controller
    {
        private readonly IRepository<string, CompanyEntity> _companyRepository;

        private readonly IRepository<int, JobOfferEntity> _jobOfferCrudRepository;
        private readonly IJobRepository _jobOfferQueryRepository;

        private readonly IClock _clock;

        private readonly IApplicantsManager _applicantsManager;

        public JobOfferController(IRepository<int, JobOfferEntity> jobOfferCrudRepository,
            IClock clock,
            IApplicantsManager applicantsManager,
            IJobRepository jobOfferQueryRepository,
            IRepository<string, CompanyEntity> companyRepository)
        {
            _companyRepository = companyRepository;
            _jobOfferCrudRepository = jobOfferCrudRepository;
            _jobOfferQueryRepository = jobOfferQueryRepository;
            _clock = clock;
            _applicantsManager = applicantsManager;
        }

        public IActionResult Index()
        {
            ViewData["CurrentUserId"] = ClaimsHelper.GetUserID(User.Claims);
            ViewData["IsCompany"] = _companyRepository.GetByPK(ClaimsHelper.GetUserID(User.Claims)) != null;

            //Javascript outlet
            return View();
        }

        public IActionResult Create()
        {
            if (_companyRepository.GetByPK(ClaimsHelper.GetUserID(User.Claims)) == null)
                return BadRequest("Only Companies Can create offers");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Title,Description,OfferActiveThrough")] JobOfferCreate JobOfferCreate)
        {
            if (_companyRepository.GetByPK(ClaimsHelper.GetUserID(User.Claims)) == null)
                return BadRequest("Only Companies Can create offers");

            if (!ModelState.IsValid)
            {
                return View(JobOfferCreate);
            }
            var userID = ClaimsHelper.GetUserID(User.Claims);
            await _jobOfferCrudRepository.AddOrUpdateAsync(JobOfferCreate.ToEntity(_clock.Now, userID));

            return RedirectToAction(nameof(Index));
        }
        /// <summary>
        /// Endpoint for infinte scroll
        /// </summary>
        /// <param name="searchString">Search string, if absent no filtering is done</param>
        /// <param name="alreadyLoadedItems">Number of records to ski[</param>
        /// <returns></returns>
        [HttpGet("api/JobOffers/Search/")]
        public async Task<ActionResult<IEnumerable<JobOfferEntity>>> Search(string searchString, int alreadyLoadedItems)
        {
            var jobs = await _jobOfferQueryRepository.GetRangeAsync(alreadyLoadedItems, 50, searchString, ClaimsHelper.GetUserID(User.Claims));
            return Json(jobs);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            if (_companyRepository.GetByPK(ClaimsHelper.GetUserID(User.Claims)) == null)
                return BadRequest("Only Companies Can create offers");

            JobOfferEntity jobOfferEntity = await _jobOfferCrudRepository.GetByPKAsync(id.Value);

            if (jobOfferEntity == null)
                return NotFound();

            if (jobOfferEntity.CreatorId != ClaimsHelper.GetUserID(User.Claims))
                return Forbid(); //Not your offer

            return View(jobOfferEntity.ToEdit());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,Description,Hidden")] JobOfferEdit jobOfferEdit)
        {
            if (id != jobOfferEdit.Id)
                return NotFound();

            if (!ModelState.IsValid)
                return View(jobOfferEdit);

            if (_companyRepository.GetByPK(ClaimsHelper.GetUserID(User.Claims)) == null)
                return BadRequest("Only Companies Can create offers");

            var job = await _jobOfferCrudRepository.GetByPKAsync(id);
            if (job.CreatorId != ClaimsHelper.GetUserID(User.Claims))
                return BadRequest("Not your offer");

            try
            {
                await _jobOfferCrudRepository.AddOrUpdateAsync(jobOfferEdit.ToEntity());
            }
            catch (DbUpdateConcurrencyException)
            {
                //TODO LOG!!
                //Dont be suprised if this fails
                return await Edit(jobOfferEdit.Id, jobOfferEdit);
            }

            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// Returns aplications for a given job
        /// </summary>
        /// <param name="id">Job id</param>
        /// <returns>Profiles of aplicants</returns>
        [HttpGet("api/JobOffers/Applications/{id}")]
        public async Task<ActionResult<IEnumerable<ProfileEntity>>> Applications(int? id)
        {
            if (id == null)
                return NotFound();

            JobOfferEntity jobOfferEntity = await _jobOfferCrudRepository.GetByPKAsync(id.Value);

            if (jobOfferEntity == null)
                return NotFound();

            if (jobOfferEntity.CreatorId != ClaimsHelper.GetUserID(User.Claims))
                return Forbid(); //Not your offer

            return Json(_applicantsManager.GetApplicants(id.Value));
        }

        [HttpPost]
        public IActionResult Apply(int id)
        {
            _applicantsManager.Apply(ClaimsHelper.GetUserID(User.Claims), id);
            return Ok();
        }

    }
}
