﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCV.Helpers
{
    public static class AppSettingFactory
    {
        public const string JsonPath = "Authentication";

        public static IConfiguration GetAppSettingsSection(this IConfiguration configuration)
        {
            return configuration.GetSection(JsonPath);
        }

        public static AppSettings GetAppSettings(this IConfiguration configuration)
        {
            return configuration.GetAppSettingsSection().Get<AppSettings>();
        }
    }
}
