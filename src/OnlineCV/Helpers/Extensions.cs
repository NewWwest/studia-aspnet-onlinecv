﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace OnlineCV.Helpers
{
    public static class Extensions
    {
        public const string JsonPath = "Authentication";

        public const string objectIdentifierClaim = "http://schemas.microsoft.com/identity/claims/objectidentifier";

        public static IConfiguration GetAppSettingsConfiguration(this IConfiguration configuration)
        {
            return configuration.GetSection(JsonPath);
        }

        public static AppSettings GetAppSettingsObject(this IConfiguration configuration)
        {
            return configuration.GetAppSettingsConfiguration().Get<AppSettings>();
        }

        public static string GetUserObjectId(this ClaimsPrincipal user)
        {
            return user.Claims.FirstOrDefault(c => c.Type == objectIdentifierClaim).Value;
        }


    }
}
