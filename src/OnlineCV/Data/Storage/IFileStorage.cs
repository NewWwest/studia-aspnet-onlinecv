﻿using System.IO;
using System.Threading.Tasks;

namespace OnlineCV.Data.Storage
{
    public interface IFileStorage
    {
        Task Save(Stream stream, string name);

        Stream Get(string name);

        Task Delete(string fileName);
    }
}
