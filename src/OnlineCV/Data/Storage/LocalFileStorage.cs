﻿using Microsoft.AspNetCore.Http;
using OnlineCV.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCV.Data.Storage
{
    public class LocalFileStorage : IFileStorage
    {
        [Obsolete]
        public Task Delete(string fileName)
        {
            throw new NotImplementedException();
        }

        [Obsolete]
        public Stream Get(string name)
        {
            return new FileStream(PathConstructor.PathToFile(name), FileMode.Open);
        }

        [Obsolete]
        public async Task Save(Stream stream, string name)
        {
            using (var file = new FileStream(PathConstructor.PathToFile(name), FileMode.Create))
            {
                 await stream.CopyToAsync(file);
            }
        }
    }
}
