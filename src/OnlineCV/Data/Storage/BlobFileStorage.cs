﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCV.Data.Storage
{
    public class BlobFileStorage : IFileStorage
    {
        private readonly StorageCredentials _storageCredentials;

        private readonly string _storageContainer;

        public BlobFileStorage(IConfiguration configuration)
        {
            _storageCredentials = new StorageCredentials(configuration["Blob:AccountName"], configuration["Blob:Key"]);
            _storageContainer = "files";
        }

        public Stream Get(string fileName)
        {
            CloudBlockBlob blockBlob = GetBlob(fileName);

            Stream target = new MemoryStream();
            blockBlob.DownloadToStream(target);

            return target;
        }

        public async Task Save(Stream stream, string fileName)
        {
            CloudBlockBlob blockBlob = GetBlob(fileName);

            await blockBlob.UploadFromStreamAsync(stream);
        }

        public async Task Delete(string fileName)
        {
            CloudBlockBlob blockBlob = GetBlob(fileName);

            await blockBlob.DeleteIfExistsAsync();
        }

        //Ref: https://docs.microsoft.com/en-us/azure/storage/blobs/storage-upload-process-images?tabs=dotnet
        private CloudBlockBlob GetBlob(string fileName)
        {
            CloudStorageAccount storageAccount = new CloudStorageAccount(_storageCredentials, true);

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            // Get reference to the blob container by passing the name by reading the value from the configuration (appsettings.json)
            CloudBlobContainer container = blobClient.GetContainerReference(_storageContainer);

            // Get the reference to the block blob from the container
            return container.GetBlockBlobReference(fileName);
        }
    }
}
