﻿using Microsoft.EntityFrameworkCore;
using OnlineCV.Models.Company;
using OnlineCV.Models.JobContenders;
using OnlineCV.Models.JobOffer;
using OnlineCV.Models.Profile;
using System.Threading.Tasks;

namespace OnlineCV.Data
{
    public class DatabaseContext : DbContext, IDatabaseContext
    {
        public DbSet<ProfileEntity> Profiles { get; set; }

        public DbSet<JobOfferEntity> JobOffers { get; set; }

        public DbSet<CompanyEntity> Companies { get; set; }

        public DbSet<JobContender> JobContenders { get; set; }

        public DatabaseContext(DbContextOptions<DatabaseContext> options) :base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Ref: https://docs.microsoft.com/en-us/ef/core/modeling/indexes
            //Indexes can not be created using data annotations.
            modelBuilder.Entity<JobOfferEntity>()
                .HasIndex(j => j.Title);

            base.OnModelCreating(modelBuilder);
        }

        void IDatabaseContext.SaveChanges()
        {
            this.SaveChanges();
        }

        Task IDatabaseContext.SaveChangesAsync()
        {
            return this.SaveChangesAsync();
        }
    }
}
