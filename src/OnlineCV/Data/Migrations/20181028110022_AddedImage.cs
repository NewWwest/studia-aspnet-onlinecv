﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OnlineCV.Migrations
{
    public partial class AddedImage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Path",
                table: "Cvs",
                newName: "ImagePath");

            migrationBuilder.AddColumn<string>(
                name: "CvPath",
                table: "Cvs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CvPath",
                table: "Cvs");

            migrationBuilder.RenameColumn(
                name: "ImagePath",
                table: "Cvs",
                newName: "Path");
        }
    }
}
