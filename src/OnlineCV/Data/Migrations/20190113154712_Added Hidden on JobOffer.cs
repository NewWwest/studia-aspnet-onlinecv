﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OnlineCV.Migrations
{
    public partial class AddedHiddenonJobOffer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Hidden",
                table: "JobOffers",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Hidden",
                table: "JobOffers");
        }
    }
}
