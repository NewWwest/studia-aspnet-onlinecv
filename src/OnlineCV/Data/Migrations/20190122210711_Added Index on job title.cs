﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OnlineCV.Migrations
{
    public partial class AddedIndexonjobtitle : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Title",
                table: "JobOffers",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_JobOffers_Title",
                table: "JobOffers",
                column: "Title");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_JobOffers_Title",
                table: "JobOffers");

            migrationBuilder.AlterColumn<string>(
                name: "Title",
                table: "JobOffers",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
