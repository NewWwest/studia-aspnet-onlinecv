﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OnlineCV.Migrations
{
    public partial class AddedlinkbetweenjobOffersandcompany : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "CreatorId",
                table: "JobOffers",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "JobContenders",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.CreateIndex(
                name: "IX_JobOffers_CreatorId",
                table: "JobOffers",
                column: "CreatorId");

            migrationBuilder.CreateIndex(
                name: "IX_JobContenders_UserId",
                table: "JobContenders",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_JobContenders_Profiles_UserId",
                table: "JobContenders",
                column: "UserId",
                principalTable: "Profiles",
                principalColumn: "UserID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_JobOffers_Companies_CreatorId",
                table: "JobOffers",
                column: "CreatorId",
                principalTable: "Companies",
                principalColumn: "UserID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JobContenders_Profiles_UserId",
                table: "JobContenders");

            migrationBuilder.DropForeignKey(
                name: "FK_JobOffers_Companies_CreatorId",
                table: "JobOffers");

            migrationBuilder.DropIndex(
                name: "IX_JobOffers_CreatorId",
                table: "JobOffers");

            migrationBuilder.DropIndex(
                name: "IX_JobContenders_UserId",
                table: "JobContenders");

            migrationBuilder.AlterColumn<string>(
                name: "CreatorId",
                table: "JobOffers",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "JobContenders",
                nullable: false,
                oldClrType: typeof(string));
        }
    }
}
