﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OnlineCV.Migrations
{
    public partial class AddedUserJobcrossTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "JobContenders",
                columns: table => new
                {
                    JobId = table.Column<int>(nullable: false),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobContenders", x => x.JobId);
                    table.ForeignKey(
                        name: "FK_JobContenders_JobOffers_JobId",
                        column: x => x.JobId,
                        principalTable: "JobOffers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "JobContenders");
        }
    }
}
