﻿using Microsoft.EntityFrameworkCore;
using OnlineCV.Models.Company;
using OnlineCV.Models.JobContenders;
using OnlineCV.Models.JobOffer;
using OnlineCV.Models.Profile;
using System.Threading.Tasks;

namespace OnlineCV.Data
{
    public interface IDatabaseContext
    {
        DbSet<ProfileEntity> Profiles { get; set; }

        DbSet<JobOfferEntity> JobOffers { get; set; }

        DbSet<CompanyEntity> Companies { get; set; }

        DbSet<JobContender> JobContenders { get; set; }

        void SaveChanges();

        Task SaveChangesAsync();
    }
}
