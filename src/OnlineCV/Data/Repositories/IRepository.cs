﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OnlineCV.Models.Profile;

namespace OnlineCV.Data.Repositories
{
    public interface IRepository<TPrimaryKey, TItem>
    {
        TItem GetByPK(TPrimaryKey PK);
        
        Task<TItem> GetByPKAsync(TPrimaryKey PK);

        void AddOrUpdate(TItem entity);

        Task AddOrUpdateAsync(TItem entity);

        bool DeleteIfExist(TPrimaryKey PK);

        Task<bool> DeleteIfExistAsync(TPrimaryKey PK);
    }
}
