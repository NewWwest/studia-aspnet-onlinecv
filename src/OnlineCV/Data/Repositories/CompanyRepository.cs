﻿using Microsoft.EntityFrameworkCore;
using OnlineCV.Models.Company;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCV.Data.Repositories
{
    internal class CompanyRepository : IRepository<string,CompanyEntity>
    {
        private readonly IDatabaseContext _databaseContext;

        public CompanyRepository(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public CompanyEntity GetByPK(string userID)
        {
            return _databaseContext.Companies.FirstOrDefault(c => c.UserID == userID);
        }

        public Task<CompanyEntity> GetByPKAsync(string userID)
        {
            return _databaseContext.Companies.Include(c=>c.Jobs).FirstOrDefaultAsync(c => c.UserID == userID);
        }

        public void AddOrUpdate(CompanyEntity newEntity)
        {
            if (string.IsNullOrWhiteSpace(newEntity.UserID))
                throw new Exception("UserId of related has to be provided");

            var dbEntity = _databaseContext.Companies.FirstOrDefault(c => c.UserID == newEntity.UserID);

            if(dbEntity != null)
                TranscribeData(newEntity, dbEntity);
            else
                 _databaseContext.Companies.Add(newEntity);

            _databaseContext.SaveChanges();
        }

        public async Task AddOrUpdateAsync(CompanyEntity newEntity)
        {
            if (string.IsNullOrWhiteSpace(newEntity.UserID))
                throw new Exception("UserId of related has to be provided"); ;

            var dbEntity = await _databaseContext.Companies.FirstOrDefaultAsync(c => c.UserID == newEntity.UserID);

            if (dbEntity != null)
                TranscribeData(newEntity, dbEntity);
            else
                await _databaseContext.Companies.AddAsync(newEntity);

            await _databaseContext.SaveChangesAsync();
        }

        public bool DeleteIfExist(string PK)
        {
            //DbContext is used directly
            throw new NotImplementedException();
        }

        public Task<bool> DeleteIfExistAsync(string PK)
        {
            //DbContext is used directly
            throw new NotImplementedException();
        }

        private void TranscribeData(CompanyEntity newEntity, CompanyEntity dbEntity)
        {
            dbEntity.Description = newEntity.Description;
            dbEntity.DisplayName = newEntity.DisplayName;
            dbEntity.Url = newEntity.Url;
        }
    }
}
