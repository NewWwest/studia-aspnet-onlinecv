﻿using OnlineCV.Models.JobOffer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCV.Data.Repositories
{
    public interface IJobRepository
    {
        Task<IEnumerable<JobOfferView>> GetRangeAsync(int skip, int take, string filter, string userId);
    }
}
