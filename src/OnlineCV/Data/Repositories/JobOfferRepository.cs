﻿using Microsoft.EntityFrameworkCore;
using OnlineCV.Models.JobOffer;
using OnlineCV.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCV.Data.Repositories
{
    internal class JobOfferRepository : IRepository<int, JobOfferEntity>, IJobRepository
    {
        private readonly IDatabaseContext _databaseContext;
        private readonly IClock _clock;

        public JobOfferRepository(IDatabaseContext databaseContext, IClock clock)
        {
            _databaseContext = databaseContext;
            _clock = clock;
        }

        public JobOfferEntity GetByPK(int PK)
        {
            return _databaseContext.JobOffers.FirstOrDefault(p => p.Id == PK);
        }

        public Task<JobOfferEntity> GetByPKAsync(int PK)
        {
            return _databaseContext.JobOffers.FirstOrDefaultAsync(p => p.Id == PK);
        }

        public void AddOrUpdate(JobOfferEntity newEntity)
        {
            var dbEntity = _databaseContext.JobOffers.FirstOrDefault(p => p.Id == newEntity.Id);

            if (dbEntity != null)
                TranscribeData(newEntity, dbEntity);
            else
                _databaseContext.JobOffers.Add(newEntity);

            _databaseContext.SaveChanges();
        }

        public async Task AddOrUpdateAsync(JobOfferEntity newEntity)
        {
            var dbEntity = await _databaseContext.JobOffers.FirstOrDefaultAsync(p => p.Id == newEntity.Id);

            if (dbEntity != null)
                TranscribeData(newEntity, dbEntity);
            else
                await _databaseContext.JobOffers.AddAsync(newEntity);

            await _databaseContext.SaveChangesAsync();
        }

        bool IRepository<int, JobOfferEntity>.DeleteIfExist(int PK)
        {
            var dbEntity = _databaseContext.JobOffers.FirstOrDefault(p => p.Id == PK);
            if (dbEntity != null)
            {
                _databaseContext.JobOffers.Remove(dbEntity);
                _databaseContext.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        async Task<bool> IRepository<int, JobOfferEntity>.DeleteIfExistAsync(int PK)
        {
            var dbEntity = _databaseContext.JobOffers.FirstOrDefault(p => p.Id == PK);
            if (dbEntity != null)
            {
                _databaseContext.JobOffers.Remove(dbEntity);
                await _databaseContext.SaveChangesAsync();
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<IEnumerable<JobOfferView>> GetRangeAsync(int skip, int take, string filter, string userId)
        {
            IQueryable<JobOfferEntity> source = string.IsNullOrWhiteSpace(filter)
                ? _databaseContext.JobOffers
                : _databaseContext.JobOffers.Where(j => j.Title.Contains(filter));

            return await source
                .Where(j => !j.Hidden && j.DueDate > _clock.Now && !j.Contenders.Select(jc=>jc.UserId).Contains(userId))
                .OrderBy(jo => jo.Id)
                .Skip(skip)
                .Take(take)
                .Include(j => j.Company)
                .Select(j => j.ToView(j.Company))
                .ToListAsync();
        }

        private void TranscribeData(JobOfferEntity newEntity, JobOfferEntity dbEntity)
        {
            dbEntity.Description = newEntity.Description;
            dbEntity.Title = newEntity.Title;
            dbEntity.Hidden = newEntity.Hidden;
        }
    }
}
