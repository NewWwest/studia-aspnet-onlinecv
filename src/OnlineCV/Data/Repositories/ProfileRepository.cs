﻿using Microsoft.EntityFrameworkCore;
using OnlineCV.Models.Profile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCV.Data.Repositories
{
    internal class ProfileRepository : IRepository<string, ProfileEntity>
    {
        private readonly IDatabaseContext _databaseContext;

        public ProfileRepository(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public void AddOrUpdate(ProfileEntity newEntity)
        {
            if (string.IsNullOrWhiteSpace(newEntity.UserID))
                throw new Exception("UserId of related has to be provided");

            var dbEntity = _databaseContext.Profiles.FirstOrDefault(p => p.UserID == newEntity.UserID);

            if (dbEntity != null)
                TranscribeData(newEntity, dbEntity);
            else
                _databaseContext.Profiles.Add(newEntity);

            _databaseContext.SaveChanges();
        }


        public async Task AddOrUpdateAsync(ProfileEntity newEntity)
        {
            if (string.IsNullOrWhiteSpace(newEntity.UserID))
                throw new Exception("UserId of related has to be provided");

            var dbEntity = await _databaseContext.Profiles.FirstOrDefaultAsync(p => p.UserID == newEntity.UserID);

            if (dbEntity != null)
                TranscribeData(newEntity, dbEntity);
            else
                await _databaseContext.Profiles.AddAsync(newEntity);

            await _databaseContext.SaveChangesAsync();
        }

        public ProfileEntity GetByPK(string userID)
        {
            return _databaseContext.Profiles.FirstOrDefault(p => p.UserID == userID);
        }

        public Task<ProfileEntity> GetByPKAsync(string userID)
        {
            return _databaseContext.Profiles.FirstOrDefaultAsync(p => p.UserID == userID);
        }

        private static void TranscribeData(ProfileEntity newEntity, ProfileEntity dbEntity)
        {
            dbEntity.Name = newEntity.Name;
            dbEntity.PathToCv = newEntity.PathToCv;
            dbEntity.PathToProfilePicture = newEntity.PathToProfilePicture;
            dbEntity.Surname = newEntity.Surname;
        }

        public bool DeleteIfExist(string PK)
        {
            //DbContext is used directly
            throw new NotImplementedException();
        }

        public Task<bool> DeleteIfExistAsync(string PK)
        {
            //DbContext is used directly
            throw new NotImplementedException();
        }
    }
}
