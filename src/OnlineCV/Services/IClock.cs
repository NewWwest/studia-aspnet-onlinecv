﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCV.Services
{
    public interface IClock
    {
        DateTime Now { get; }
    }

    public class DateTimeClock : IClock
    {
        public DateTime Now => DateTime.UtcNow;
    }
}
