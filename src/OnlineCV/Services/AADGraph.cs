﻿using OnlineCV.Models.AAD;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.IdentityModel.Clients.ActiveDirectory;

namespace OnlineCV.Services
{
    public class AADGraph
    {
        private AuthenticationContext _authContext;
        private ClientCredential _clientCredential;
        const string AAD_GRAPH_URI = "https://graph.windows.net";
        AppSettings _appSetings;

        public AADGraph(AppSettings appSettings)
        {
            _appSetings = appSettings;
            _authContext = new AuthenticationContext(_appSetings.AuthenticationContext);
            _clientCredential = new ClientCredential(_appSetings.GroupApplicationId, _appSetings.GroupApplicationKey);
        }

        public async Task<bool> IsUserInGroup(IEnumerable<Claim> userClaims, string groupId)
        {         
            AuthenticationResult token = await _authContext.AcquireTokenAsync(AAD_GRAPH_URI, _clientCredential);
                        
            var ID_CLAIM_NAME = _appSetings.UserIdClaimName;

            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(token.AccessTokenType, token.AccessToken);

            var queryString = HttpUtility.ParseQueryString(string.Empty);
            queryString["api-version"] = "1.6";
            var uri = AAD_GRAPH_URI + "/myorganization/groups/" + groupId + "/$links/members?" + queryString;

            var members = await client.GetAsync(uri);
            bool userIsInGroup = false;
            if (members.Content != null)
            {
                var responseString = await members.Content.ReadAsStringAsync();
                AADGroupMembers gm = JsonConvert.DeserializeObject<AADGroupMembers>(responseString);
                List<string> userIds = new List<string>();
                foreach (var m in gm.value)
                {
                    userIds.Add(m.url.Split("/")[5]);
                }
                foreach (Claim c in userClaims)
                {
                    if (String.Compare(c.Type, ID_CLAIM_NAME) == 0)
                    {
                        foreach (string uId in userIds)
                        {
                            if (String.Compare(c.Value, uId) == 0)
                                return true;
                        }
                        break;
                    }
                }
            }
            return userIsInGroup;
        }

        public async Task<bool> IsUserInGroup(IEnumerable<Claim> claims, AADGroups admins)
        {
            string groupId = _appSetings.AADGroups.FirstOrDefault(g => g.Name == AADGroups.Admins.ToString()).Id;
            return await IsUserInGroup(claims, groupId);
        }
    }
}
