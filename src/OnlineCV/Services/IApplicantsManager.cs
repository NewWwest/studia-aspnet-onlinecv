﻿using OnlineCV.Models.Profile;
using System.Collections.Generic;

namespace OnlineCV.Services
{
    public interface IApplicantsManager
    {
        void Apply(string userId, int jobId);
        
        IEnumerable<ProfileEntity> GetApplicants(int jobId);
    }
}