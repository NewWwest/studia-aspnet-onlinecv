﻿using OnlineCV.Data;
using OnlineCV.Data.Repositories;
using OnlineCV.Models.JobContenders;
using OnlineCV.Models.JobOffer;
using OnlineCV.Models.Profile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCV.Services
{
    public class ApplicantsManager : IApplicantsManager
    {
        private readonly IRepository<string, ProfileEntity> _profileRepository;

        private readonly IRepository<int, JobOfferEntity> _jobOfferRepository;

        private readonly IDatabaseContext _databaseContext;

        public ApplicantsManager(IRepository<int, JobOfferEntity> jobOfferRepository, IRepository<string, ProfileEntity> profileRepository, IDatabaseContext databaseContext)
        {
            _profileRepository = profileRepository;
            _jobOfferRepository = jobOfferRepository;
            _databaseContext = databaseContext;
        }

        public void Apply(string userId, int jobId)
        {
            if (_profileRepository.GetByPK(userId) == null)
                throw new Exception("Setup profile first");
            
            JobOfferEntity job = _jobOfferRepository.GetByPK(jobId);
            if (job == null)
                throw new Exception("no such job");

            JobContender dbEntry = _databaseContext.JobContenders.FirstOrDefault(jc => jc.JobId == jobId && jc.UserId == userId);
            if (dbEntry != null)
                return;

            _databaseContext.JobContenders.Add(new JobContender() { JobId = jobId, UserId = userId });
            _databaseContext.SaveChanges();

            //TODO (business)
        }

        public IEnumerable<ProfileEntity> GetApplicants(int jobId)
        {
            return _databaseContext.Profiles.Where(p => p.Applications.Select(a => a.JobId).Contains(jobId)).ToList();

        }
    }
}
