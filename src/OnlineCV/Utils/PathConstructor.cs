﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCV.Utils
{
    static public class PathConstructor
    {
        [Obsolete]
        static public string PathToFile(string name) => @"E:\Projekty\ASPNET\OnlineCV\src\OnlineCV\wwwroot\Files\" + name;

        [Obsolete]
        static public string HttpToFile(string name) => $"/wwwroot/Files/{name}";

        static public string PathToUri(string pathToFile) => $"https://onlinecvxxx446ad70be94.blob.core.windows.net/files/{pathToFile}";
    }
}
