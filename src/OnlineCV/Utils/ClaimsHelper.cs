﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace OnlineCV.Utils
{
    public class ClaimsHelper
    {
        private const string NameIdentifierSubject = @"http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier";
        private const string EmailIdentifierSubject = @"emails";

        //+		[0]	{http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier: 634c159c-9bdf-4a72-bd55-73dd3770c1fb}	System.Security.Claims.Claim
        //+		[1]	{http://schemas.microsoft.com/identity/claims/objectidentifier: 634c159c-9bdf-4a72-bd55-73dd3770c1fb}	System.Security.Claims.Claim
        //+		[2]	{name: AnJ West}System.Security.Claims.Claim
        //+		[3]	{emails: anjwest0 @gmail.com}System.Security.Claims.Claim
        //+		[4]	{tfp: B2C_1_BasicSignIn}	System.Security.Claims.Claim
        public static string GetUserID(IEnumerable<Claim> claims)
        {
            foreach (var claim in claims)
            {
                if (claim.Type.ToString() == NameIdentifierSubject)
                    return claim.Value;
            }
            return string.Empty;
        }

        public static string GetUserEmail(IEnumerable<Claim> claims)
        {
            foreach (var claim in claims)
            {
                if (claim.Type.ToString() == EmailIdentifierSubject)
                    return claim.Value;
            }
            return string.Empty;
        }

    }
}
