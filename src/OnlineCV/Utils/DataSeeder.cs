﻿using OnlineCV.Data;
using OnlineCV.Models.Company;
using OnlineCV.Models.JobOffer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCV.Utils
{
    public class DataSeeder
    {
        private static Random rand = new Random();

        private readonly IDatabaseContext _dataContext;

        public DataSeeder(IDatabaseContext dataContext)
        {
            _dataContext = dataContext;
        }

        private static string loremIpsum =
            @"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Faucibus ornare suspendisse sed nisi. Nunc faucibus a pellentesque sit amet porttitor eget. Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi tristique. Aliquam id diam maecenas ultricies mi. Elit ut aliquam purus sit amet. Adipiscing enim eu turpis egestas pretium aenean pharetra magna. Habitant morbi tristique senectus et netus et. Euismod in pellentesque massa placerat duis ultricies lacus sed. Arcu odio ut sem nulla pharetra diam sit amet. Leo urna molestie at elementum.
            Dignissim suspendisse in est ante in nibh. Blandit aliquam etiam erat velit. Scelerisque fermentum dui faucibus in ornare quam viverra orci sagittis. Sit amet mattis vulputate enim nulla aliquet. Euismod quis viverra nibh cras. Turpis egestas sed tempus urna et pharetra pharetra massa. Amet purus gravida quis blandit turpis cursus in. Sem integer vitae justo eget magna fermentum iaculis eu non. Massa ultricies mi quis hendrerit dolor magna eget est lorem. Magna ac placerat vestibulum lectus mauris ultrices. Vulputate eu scelerisque felis imperdiet proin fermentum. Urna duis convallis convallis tellus id interdum velit. Mattis aliquam faucibus purus in. Quis vel eros donec ac odio tempor orci. Lectus nulla at volutpat diam ut. Dictum fusce ut placerat orci nulla pellentesque dignissim enim sit. Nunc sed id semper risus. Facilisi cras fermentum odio eu feugiat pretium nibh. Eget lorem dolor sed viverra ipsum nunc aliquet bibendum. Cursus mattis molestie a iaculis at erat pellentesque.";

        public void SeedJobOffers()
        {
            var UserIds = Enumerable.Repeat(0, 50).Select(i => Guid.NewGuid().ToString()).ToList();
            for (int i = 0; i < 50; i++)
            {
                var temp = new CompanyEntity()
                {
                    Description = loremIpsum.Substring(0, rand.Next(100, loremIpsum.Length)),
                    DisplayName = Guid.NewGuid().ToString(),
                    Url = Guid.NewGuid().ToString(),
                    UserID = UserIds[i]
                };
                _dataContext.Companies.Add(temp);
            }
            for (int i = 0; i < 200; i++)
            {
                var temp = new JobOfferEntity()
                {
                    CreateDate = DateTime.Now.AddDays((rand.NextDouble() * 10) - 5),
                    DueDate = DateTime.Now.AddDays(rand.NextDouble() * 100),
                    CreatorId = UserIds[rand.Next(0, 50)],
                    Description = loremIpsum.Substring(0, rand.Next(100, loremIpsum.Length)),
                    Title = Guid.NewGuid().ToString()
                };
                _dataContext.JobOffers.Add(temp);
            }
            _dataContext.SaveChanges();
        }
    }
}

