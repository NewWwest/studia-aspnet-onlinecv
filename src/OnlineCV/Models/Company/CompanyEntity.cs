﻿using OnlineCV.Models.JobOffer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCV.Models.Company
{
    /// <summary>
    /// Contains additional informations about thr user that posts job offers
    /// /// </summary>
    public class CompanyEntity
    {
        [Key]
        public string UserID { get; set; }

        public string DisplayName { get; set; }

        public string Description { get; set; }

        public string Url { get; set; }

        public virtual List<JobOfferEntity> Jobs { get; set; }
    }
}
