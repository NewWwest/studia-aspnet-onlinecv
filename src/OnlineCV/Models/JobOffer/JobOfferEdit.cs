﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCV.Models.JobOffer
{
    public class JobOfferEdit
    {
        public int Id { get; set; }

        [Required()]
        [MaxLength(100)]
        public string Title { get; set; }

        [Required()]
        [MaxLength(1000)]
        public string Description { get; set; }

        [Required()]
        public bool Hidden { get; set; }

        public DateTime DueDate { get; set; }
        
        public JobOfferEntity ToEntity()
        {
            return new JobOfferEntity()
            {
                Id = Id,
                Title = Title,
                Description = Description,
                Hidden = Hidden
            };
        }
    }
}
