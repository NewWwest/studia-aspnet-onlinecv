﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCV.Models.JobOffer
{
    public class JobOfferCreate
    {
        [Required()]
        [MaxLength(100)]
        public string Title { get; set; }

        [Required()]
        [MaxLength(1000)]
        public string Description { get; set; }

        [Required()]
        public ActiveThrough OfferActiveThrough { get; set; }

        internal JobOfferEntity ToEntity(DateTime currentDate, string creatorID)
        {
            JobOfferEntity job = new JobOfferEntity()
            {
                CreateDate = currentDate,
                CreatorId = creatorID,
                Description = this.Description,
                DueDate = GetDueDate(currentDate),
                Title = this.Title
            };
            return job;
        }

        private DateTime GetDueDate(DateTime currentDate)
        {
            switch (OfferActiveThrough)
            {
                case ActiveThrough.Week:
                    return currentDate.AddDays(7);
                case ActiveThrough.Month:
                    return currentDate.AddMonths(1);
                case ActiveThrough.ThreeMonths:
                    return currentDate.AddMonths(3);
                case ActiveThrough.SixMonths:
                    return currentDate.AddMonths(6);
                default:
                    throw new ArgumentOutOfRangeException(OfferActiveThrough.ToString());
            }
        }
    }
}