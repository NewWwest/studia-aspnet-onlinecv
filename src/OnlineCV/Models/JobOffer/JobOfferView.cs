﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCV.Models.JobOffer
{
    public class JobOfferView
    {
        public int Id { get; set; }

        public string CreatorId { get; set; }

        public string Creator { get; set; }

        public string CreatorDescription { get; set; }

        public string CreatorUrl { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }
    }
}
