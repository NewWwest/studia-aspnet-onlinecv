﻿using OnlineCV.Models.Company;
using OnlineCV.Models.JobContenders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCV.Models.JobOffer
{
    public class JobOfferEntity
    {
        public int Id { get; set; }

        public string CreatorId { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime DueDate { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public bool Hidden { get; set; }

        public virtual List<JobContender> Contenders { get; set; }

        [ForeignKey(nameof(CreatorId))]
        public virtual CompanyEntity Company { get; set; }

        public JobOfferView ToView(CompanyEntity company)
        {
            return new JobOfferView()
            {
                Id = Id,
                CreatorId=CreatorId,
                Creator=company.DisplayName,
                CreatorDescription=company.Description,
                CreatorUrl= company.Url,
                Description = Description,
                Title = Title
            };
        }

        public JobOfferEdit ToEdit()
        {
            return new JobOfferEdit()
            {
                Id = Id,
                Description = Description,
                DueDate = DueDate,
                Hidden = Hidden,
                Title = Title
            };
        }
    }
}
