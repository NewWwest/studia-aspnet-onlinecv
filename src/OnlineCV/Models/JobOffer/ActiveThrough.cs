﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCV.Models.JobOffer
{
    public enum ActiveThrough
    {
        Week,
        Month,
        ThreeMonths,
        SixMonths
    }
}
