﻿using OnlineCV.Models.JobOffer;
using OnlineCV.Models.Profile;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OnlineCV.Models.JobContenders
{
    public class JobContender
    {
        [Key]
        public int JobId { get; set; }

        [Required]
        public string UserId { get; set; }

        [ForeignKey(nameof(JobId))]
        public virtual JobOfferEntity JobOffer { get; set; }

        [ForeignKey(nameof(UserId))]
        public virtual ProfileEntity Profile { get; set; }
    }
}
