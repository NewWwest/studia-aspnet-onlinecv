﻿using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using OnlineCV.Utils;

namespace OnlineCV.Models.Profile
{
    public class ProfileCreate
    {
        public string Name { get; set; }

        public string Surname { get; set; }

        public IFormFile Cv { get; set; }

        public IFormFile ProfileImage { get; set; }

        public bool IsValid()
        {
            if (string.IsNullOrWhiteSpace(Name) || string.IsNullOrWhiteSpace(Surname))
                return false;

            if (!FileTypeUtils.IsImage(ProfileImage)) 
                return false;

            if (!FileTypeUtils.IsPdfOrHtmlOrTxt(Cv))
                return false;

            //Good enough
            return true;
        }

        public ProfileEntity ToEntity(string UserID)
        {
            return new ProfileEntity()
            {
                UserID = UserID,
                Name = Name,
                Surname = Surname,
                PathToCv = Name + "_" + Surname + "_" + Guid.NewGuid().ToString() + "_cv" + Path.GetExtension(Cv.ContentType),
                PathToProfilePicture = Name + "_" + Surname + Guid.NewGuid().ToString() + "_img" + Path.GetExtension(ProfileImage.ContentType),
            };
        }
    }
}
