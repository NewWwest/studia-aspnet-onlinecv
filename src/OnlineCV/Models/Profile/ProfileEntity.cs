﻿using OnlineCV.Models.JobContenders;
using OnlineCV.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCV.Models.Profile
{
    public class ProfileEntity
    {
        [Key]
        public string UserID { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string PathToCv { get; set; }

        public string PathToProfilePicture { get; set; }

        public virtual List<JobContender> Applications { get; set; }

        public ProfileView ToProfileView()
        {
            return new ProfileView()
            {
                Name = Name,
                Surname = Surname,
                UriToCv = PathConstructor.PathToUri(PathToCv),
                UriToProfilePicture = PathConstructor.PathToUri(PathToProfilePicture)
            };
        }
    }
}
