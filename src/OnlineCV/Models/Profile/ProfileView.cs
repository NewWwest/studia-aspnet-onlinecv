﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCV.Models.Profile
{
    public class ProfileView
    {
        public string Name { get; set; }

        public string Surname { get; set; }

        public string UriToCv { get; set; }

        public string UriToProfilePicture { get; set; }
    }
}
