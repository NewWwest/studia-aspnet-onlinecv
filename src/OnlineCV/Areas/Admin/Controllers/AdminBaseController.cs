﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using OnlineCV.Helpers;
using OnlineCV.Models.AAD;
using OnlineCV.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace OnlineCV.Areas.Admin.Controllers
{
    [Authorize]
    [Area("Admin")]
    public class AdminBaseController : Controller
    {
        protected readonly AADGraph _AADGraph;

        public AdminBaseController(IConfiguration configuration)
        {
            _AADGraph = new AADGraph(configuration.GetAppSettings());
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!_AADGraph.IsUserInGroup(User.Claims, AADGroups.Admins).Result)
            {
                filterContext.Result = Forbid();
                return;
            }
               
            base.OnActionExecuting(filterContext);
        }
    }
}
