﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using OnlineCV.Data;
using OnlineCV.Models.Profile;

namespace OnlineCV.Areas.Admin.Controllers
{
    public class ProfilesController : AdminBaseController
    {
        private readonly DatabaseContext _context;

        public ProfilesController(DatabaseContext context, IConfiguration configuration) : base(configuration)
        {
            _context = context;
        }

        // GET: Admin/Profiles
        public async Task<IActionResult> Index()
        {
            return View(await _context.Profiles.ToListAsync());
        }

        // GET: Admin/Profiles/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var profileEntity = await _context.Profiles
                .FirstOrDefaultAsync(m => m.UserID == id);
            if (profileEntity == null)
            {
                return NotFound();
            }

            return View(profileEntity);
        }

        // GET: Admin/Profiles/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/Profiles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("UserID,Name,Surname,PathToCv,PathToProfilePicture")] ProfileEntity profileEntity)
        {
            if (ModelState.IsValid)
            {
                _context.Add(profileEntity);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(profileEntity);
        }

        // GET: Admin/Profiles/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var profileEntity = await _context.Profiles.FindAsync(id);
            if (profileEntity == null)
            {
                return NotFound();
            }
            return View(profileEntity);
        }

        // POST: Admin/Profiles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("UserID,Name,Surname,PathToCv,PathToProfilePicture")] ProfileEntity profileEntity)
        {
            if (id != profileEntity.UserID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(profileEntity);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProfileEntityExists(profileEntity.UserID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(profileEntity);
        }

        // GET: Admin/Profiles/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var profileEntity = await _context.Profiles
                .FirstOrDefaultAsync(m => m.UserID == id);
            if (profileEntity == null)
            {
                return NotFound();
            }

            return View(profileEntity);
        }

        // POST: Admin/Profiles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var profileEntity = await _context.Profiles.FindAsync(id);
            _context.Profiles.Remove(profileEntity);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProfileEntityExists(string id)
        {
            return _context.Profiles.Any(e => e.UserID == id);
        }
    }
}
