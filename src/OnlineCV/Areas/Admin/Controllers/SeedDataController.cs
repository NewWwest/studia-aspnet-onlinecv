﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using OnlineCV.Data;
using OnlineCV.Helpers;
using OnlineCV.Models.AAD;
using OnlineCV.Services;
using OnlineCV.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCV.Areas.Admin.Controllers
{
    public class SeedDataController : AdminBaseController
    {
        private readonly IDatabaseContext _dataContext;

        public SeedDataController(IDatabaseContext dataContext, IConfiguration configuration) : base(configuration)
        {
            _dataContext = dataContext;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult IndexPost()
        {
            var dataSeeder = new DataSeeder(_dataContext);
            dataSeeder.SeedJobOffers();
            ViewData["Message"] = "Seeding successful";
            return Redirect("/");
        }
    }
}
