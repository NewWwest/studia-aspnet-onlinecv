﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using OnlineCV.Data;
using OnlineCV.Models.JobOffer;

namespace OnlineCV.Areas.Admin.Controllers
{
    public class JobsController : AdminBaseController
    {
        private readonly DatabaseContext _context;

        public JobsController(DatabaseContext context, IConfiguration configuration) : base(configuration)
        {
            _context = context;
        }

        // GET: Admin/Jobs
        public async Task<IActionResult> Index()
        {
            var databaseContext = _context.JobOffers.Include(j => j.Company);
            return View(await databaseContext.ToListAsync());
        }

        // GET: Admin/Jobs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var jobOfferEntity = await _context.JobOffers
                .Include(j => j.Company)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (jobOfferEntity == null)
            {
                return NotFound();
            }

            return View(jobOfferEntity);
        }

        // GET: Admin/Jobs/Create
        public IActionResult Create()
        {
            ViewData["CreatorId"] = new SelectList(_context.Companies, "UserID", "UserID");
            return View();
        }

        // POST: Admin/Jobs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,CreatorId,CreateDate,DueDate,Title,Description,Hidden")] JobOfferEntity jobOfferEntity)
        {
            if (ModelState.IsValid)
            {
                _context.Add(jobOfferEntity);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CreatorId"] = new SelectList(_context.Companies, "UserID", "UserID", jobOfferEntity.CreatorId);
            return View(jobOfferEntity);
        }

        // GET: Admin/Jobs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var jobOfferEntity = await _context.JobOffers.FindAsync(id);
            if (jobOfferEntity == null)
            {
                return NotFound();
            }
            ViewData["CreatorId"] = new SelectList(_context.Companies, "UserID", "UserID", jobOfferEntity.CreatorId);
            return View(jobOfferEntity);
        }

        // POST: Admin/Jobs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,CreatorId,CreateDate,DueDate,Title,Description,Hidden")] JobOfferEntity jobOfferEntity)
        {
            if (id != jobOfferEntity.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(jobOfferEntity);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!JobOfferEntityExists(jobOfferEntity.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CreatorId"] = new SelectList(_context.Companies, "UserID", "UserID", jobOfferEntity.CreatorId);
            return View(jobOfferEntity);
        }

        // GET: Admin/Jobs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var jobOfferEntity = await _context.JobOffers
                .Include(j => j.Company)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (jobOfferEntity == null)
            {
                return NotFound();
            }

            return View(jobOfferEntity);
        }

        // POST: Admin/Jobs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var jobOfferEntity = await _context.JobOffers.FindAsync(id);
            _context.JobOffers.Remove(jobOfferEntity);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool JobOfferEntityExists(int id)
        {
            return _context.JobOffers.Any(e => e.Id == id);
        }
    }
}
