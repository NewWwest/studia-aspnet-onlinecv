﻿declare @__filter_0 nvarchar(max) ='dupa'
declare @___clock_Now_1 Date= GETDATE()
declare @__p_3 int=0
declare @__p_4 int =50
declare @__userId_2 nvarchar(max) ='dupa'
SELECT 
[j].[Id], [j].[CreateDate], [j].[CreatorId], [j].[Description], [j].[DueDate], [j].[Hidden], [j].[Title], [j.Company].[UserID], [j.Company].[Description], [j.Company].[DisplayName], [j.Company].[Url]
FROM [JobOffers] AS [j]
LEFT JOIN [Companies] AS [j.Company] ON [j].[CreatorId] = [j.Company].[UserID]
WHERE (
	(CHARINDEX(@__filter_0, [j].[Title]) > 0) OR (@__filter_0 = N'')) 
	AND (
	(([j].[Hidden] = 0) AND ([j].[DueDate] > @___clock_Now_1)) 
	AND @__userId_2 NOT IN (
		SELECT [jc].[UserId]
		FROM [JobContenders] AS [jc]
		WHERE [j].[Id] = [jc].[JobId]
	))
ORDER BY [j].[Id]
OFFSET @__p_3 ROWS FETCH NEXT @__p_4 ROWS ONLY


----Simplified where
--declare @__filter_0 nvarchar(max) ='SearchString'
--declare @___clock_Now_1 Date= GETDATE()
--declare @__p_3 int=0
--declare @__p_4 int =50
--declare @__userId_2 nvarchar(max) ='userID'
--SELECT 
--[j].[Id], [j].[CreateDate], [j].[CreatorId], [j].[Description], [j].[DueDate], [j].[Hidden], [j].[Title], [j.Company].[UserID], [j.Company].[Description], [j.Company].[DisplayName], [j.Company].[Url]
--FROM [JobOffers] AS [j]
--LEFT JOIN [Companies] AS [j.Company] ON [j].[CreatorId] = [j.Company].[UserID]
--WHERE CHARINDEX(@__filter_0, [j].[Title]) > 0
--ORDER BY [j].[Id]
--OFFSET @__p_3 ROWS FETCH NEXT @__p_4 ROWS ONLY